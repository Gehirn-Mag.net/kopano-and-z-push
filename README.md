# Kopano and z-push

You use Kopano and Z-Push? You have many public folders which you have to add? Then this script can be of help.

Just edit the configs to your needs, run the script and add just one include_once line at the bottom of your z-push conf so that it looks like this:

```
    $additionalFolders = array(
        // demo entry for the synchronization of contacts from the public folder.
        // uncomment (remove '/*' '*/') and fill in the folderid
/*
        array(
            'store'     => "SYSTEM",
            'folderid'  => "",
            'name'      => "Public Contacts",
            'type'      => SYNC_FOLDER_TYPE_USER_CONTACT,
            'flags'     => DeviceManager::FLD_FLAGS_NONE,
        ),
*/

    );

    // add my own folders ;-)
    include_once('/etc/z-push/publicfolders.php');

```

---

More details will follow on my personal blog soon: [https://gehirn-mag.net](https://gehirn-mag.net).

---

Links to Kopano and z-push:

- Kopano: [https://kopano.com/](https://kopano.com/)
- Kopano community: [https://kopano.io/](https://kopano.io/)
- z-push: [http://z-push.org/](http://z-push.org/)