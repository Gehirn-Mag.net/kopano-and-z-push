#!/usr/bin/env php
<?php

#
# z-push all public folders
#

#
# MIT License
#
# Copyright (c) 2019 Steffen Schoch <mein@gehirn-mag.net>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
#


# 2019-03-16, schoch: Init...



##### Settings - please change to your needs ###################################

$c = array(

    // listfolders.php out of the z-push package
    'listfolders'       => '/usr/share/z-push/backend/kopano/listfolders.php',
    // args needed for listfolders to connect
    'listfolders_args'  => '-l SYSTEM -u s.schoch@schoch-it.solutions -p ...',
    // php needed to execute listfolders
    'listfolders_php'   => '/usr/bin/env php',

    // ignore folders: list of folderids to ignore
    'ignore'            => array(
        '8f9b04c5e38f4a30a7eb229e85146d884c9701000000', // Folder 1
        '8f9b04c5e38f4a30a7eb229e85146d88539701000000', // Folder 2
    ),

    // save result to
    'outputfile'        => '/etc/z-push/publicfolders.php'

);

##### No changes need to be done behind here ###################################


// popen listfolders and read all items
if(!is_file($c['listfolders'])) {
    exit('Couldn\'t find listfolders.php. Please check your settings.');
}
$ph = popen(join(
    ' ',
    array($c['listfolders_php'], $c['listfolders'], $c['listfolders_args'])
    ), 'r');
if($ph === false) {
    exit('Problem executing listfolders.php. Please check your settings.');
}
$d = array();
while(!feof($ph)) {
    $line = fgets($ph);
    // search for "Folder name:"
    if(preg_match('/^Folder name:\s+(.+)$/', $line, $m)) {
        $fn = $m[1];
        preg_match('/^Folder ID:\s+(.+)$/', fgets($ph), $m);
        $fi = $m[1];
        preg_match('/^Type:\s+(.+)$/', fgets($ph), $m);
        $ty = $m[1];

        // Something went wrong? Break!
        if(!$fn && !$fi && !$ty) {
            exit('Missunderstod listfolders.php output.');
        }

        // skip if in ignore list
        if(in_array($fi, $c['ignore'])) {
            printf("SKIP %s (%s)\n", $fi, $fn);
            continue;
        }

        // add to list
        printf("ADD  %s (%s)\n", $fi, $fn);
        $d[] = array($fn, $fi, $ty);
    } else {
        continue;
    }
}
pclose($ph);

// writing output to file
print("saving...\n");
$s = '';
foreach($d as $k => $v) {
    if($s) { $s .= ','; };
    $s .= sprintf(
        'array(\'store\'=>\'SYSTEM\',\'folderid\'=>\'%s\',\'name\'=>\'%s\',\'type\'=>%s,\'flags\'=>DeviceManager::FLD_FLAGS_NONE)',
        $v[1], $v[0], $v[2]
    );
}
$fh = fopen($c['outputfile'], 'w');
if($fh === false) {
    exit('Problem writing outputfile. Please check your settings.');
}
fprintf($fh, '<?php $additionalFolders=array_merge($additionalFolders,array(%s));', $s);
